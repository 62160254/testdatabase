/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanawat.sqliteproject;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author THANAWAT_TH
 */
public class InsertUserStd {

    public static void main(String[] args) {
        Connection conn = null;
        Statement stmt = null;

        try {
            Class.forName("org.sqlite.JDBC");
            conn = DriverManager.getConnection("jdbc:sqlite:user.db");
            conn.setAutoCommit(false);
            stmt = conn.createStatement();

            String sql = "INSERT INTO UserStd (ID,NAME,AGE,ADDRESS,SALARY) "
                    + "VALUES ( 1, 'BESTKILLER',20,'THAILAND',60000.00);";
            stmt.executeUpdate(sql);

            sql = "INSERT INTO UserStd (ID,NAME,AGE,ADDRESS,SALARY) "
                    + "VALUES (2, 'STANG_THANAWAT_TH', 21, 'JAPAN', 65000.00 );";
            stmt.executeUpdate(sql);

            sql = "INSERT INTO UserStd (ID,NAME,AGE,ADDRESS,SALARY) "
                    + "VALUES (3, 'POOBODIN_BOY', 20, 'AMERICAN', 75000.00 );";
            stmt.executeUpdate(sql);

            sql = "INSERT INTO UserStd (ID,NAME,AGE,ADDRESS,SALARY) "
                    + "VALUES (4, 'AUZA',21,'CANADA', 55000.00 );";
            stmt.executeUpdate(sql);

            sql = "INSERT INTO UserStd (ID,NAME,AGE,ADDRESS,SALARY) "
                    + "VALUES (5, 'MAX',21,'AMERICAN', 65000.00 );";
            stmt.executeUpdate(sql);
            
            conn.commit();
            stmt.close();
            conn.close();
        } catch (ClassNotFoundException ex) {
            System.out.println("No Libary org.sqlite.JDBC not found");
        } catch (SQLException ex) {
            System.out.println("Unable to connect database!!!");
        }

    }
}
