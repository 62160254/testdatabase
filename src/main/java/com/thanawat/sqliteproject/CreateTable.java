/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanawat.sqliteproject;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;


/**
 *
 * @author THANAWAT_TH
 */
public class CreateTable {
    public static void main(String[] args) {
        Connection conn = null;
        String dbname = "user.db";
        Statement stmt = null;
        //Connnection
        try {
            Class.forName("org.sqlite.JDBC");
            conn = DriverManager.getConnection("jdbc:sqlite:" + dbname);
            stmt = conn.createStatement();
            String sql = "Create Table Company " + "( ID int Primary key not null,"+
                        "Name Text not null, " + "Age int not null, " + 
                        "Address Char(50), " + "Salary real)";
            stmt.executeUpdate(sql);
            stmt.close();
            conn.close();
            
            
        } catch (ClassNotFoundException ex) {
            System.out.println("No Libary org.sqlite.JDBC not found");
            System.exit(0);
        } catch (SQLException ex) {
            System.out.println("Unable to connect database!!!");
            System.exit(0);
        }
        
        
    }
}
