/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanawat.sqliteproject;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author THANAWAT_TH
 */
public class DeleteUserStd {
    public static void main(String[] args) {
        Connection conn = null;
        Statement stmt = null;
        
        try {
            Class.forName("org.sqlite.JDBC");
            conn = DriverManager.getConnection("jdbc:sqlite:user.db");
            stmt = conn.createStatement();
            
            conn.setAutoCommit(false);
            
            //DELETE
            stmt.executeUpdate("DELETE from UserStd where ID = 5");
            conn.commit();
            
            
            //select
            ResultSet rs = stmt.executeQuery("SELECT * FROM UserStd");
            
            while(rs.next()){
                int id = rs.getInt("id");
                String name = rs.getString("name");
                int age = rs.getInt("age");
                String address = rs.getString("address");
                float salary = rs.getFloat("salary");
                System.out.println("ID = " + id);
                System.out.println("NAME = " + name);
                System.out.println("AGE = " + age);
                System.out.println("ADDRESS = " + address);
                System.out.println("SALARY = " + salary);
                System.out.println("");
            }
            
            
            
            
            
            stmt.close();
            conn.close();
            
        } catch (ClassNotFoundException ex) {
            System.out.println("");
        } catch (SQLException ex) {
            System.out.println("");
        }
      
    }
}
